### push代码前请先做好充分测试.

- ### 组件命名风格  
  1. 局部组件放置在/components/widgets目录下,使用驼峰式命名风格命名.在vue模板中统一使用w-前缀小写组件名。.  
  2. 外部组件命名直接放置在/components目录下命名使用Pascal命名风格.  
  
- ### 自定义指令  
  1.放置在/directives目录下,命名统一小写. 
  2.请在文件内容顶部加上/**/注释,表明此指令作用.  
  
- ### 开发环境Mock数据配置  
  1.数据文件放置在/static/mockdata.json中.  
  2.内容格式为{"数据标识":模拟数据}  
  3.使用express搭建本地api,api接口配置在/build/webpack.dev.conf.js中.  
  
- ### 文件冲突  
   1.push代码遇到冲突请慎重处理!.
### 基于Vue.js 2.0改造blur-admin后台管理框架  

- ### 特征  
    响应式布局  
    高分辨率  
    Bootstrap CSS 框架  
    Sass  
    Gulp build  
    ~~AngularJS~~ Vue.js  
    Jquery  
    Charts (amChart, Chartist, Chart.js, Morris)  
    Maps (Google, Leaflet, amMap)  
    etc  

- ### 构建步骤

    ``` bash
    # install dependencies
    npm install
    
    # serve with hot reload at localhost:8080
    npm run dev
    
    # build for production with minification
    npm run build
    
    # build for production and view the bundle analyzer report
    npm run build --report
    
    ```

- ### License  
    MIT license.  

- ### 预览  
  原项目预览地址: [blur-admin](https://akveo.github.io/blur-admin/)  
  ![blur-admin-preview](./blur-admin-preview.png)

---
#### Forked from [akveo/blur-admin](https://github.com/akveo/blur-admin)



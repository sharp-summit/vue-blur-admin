import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import Mail from '@/components/Mail'
import TimeLine from '@/components/TimeLine'
import Empty from '@/components/Empty'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      alias:['/dashboard','/home'],
      component: Dashboard
    },
    {
      path: '/components/mail',
      name: 'Components-mail',
      component: Mail,
      alias:'/components'
    },
    {
      path: '/components/timeline',
      component: TimeLine,
    }
    // {
    //   path: '/components',
    //   name: 'Components-mail',
    //   component: Empty,
    //   //redirect:'/components/mail',
    //   children:[
    //     {
    //       path:'mail',
    //       name:'mail',
    //       component:Mail
    //     },
    //     {
    //       path:'timeline',
    //       component:TimeLine
    //     }
    //   ]
    // }
  ]
})
